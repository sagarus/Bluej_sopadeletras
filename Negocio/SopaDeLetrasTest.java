package Negocio;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class SopaDeLetrasTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class SopaDeLetrasTest
{
    /**
     * Default constructor for test class SopaDeLetrasTest
     */
    public SopaDeLetrasTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }

    /*
     * se espera crear matriz de manera exitosa
     */
    @Test
    public void crearMatriz1() throws Exception
    {
        String palabras = "juan,carlos,david,ana";
        SopaDeLetras sopa = new SopaDeLetras(palabras);

        char ideal [][] = 
            {
                {'j','u','a','n'},
                {'c','a','r','l','o','s'},
                {'d','a','v','i','d'},
                {'a','n','a'}
            };

        char esperado [][] = sopa.getSopas();  

        boolean pasoPrueba = sonIguales(ideal,esperado);
        assertEquals(true,pasoPrueba);
    }

    @Test
    public void crearMatriz2() throws Exception
    {
        String palabras = "j,c,d,a";
        SopaDeLetras sopa = new SopaDeLetras(palabras);

        char ideal [][] = 
            {
                {'j'},
                {'c'},
                {'d'},
                {'a'}
            };

        char esperado [][] = sopa.getSopas();  

        boolean pasoPrueba = sonIguales(ideal,esperado);
        assertEquals(true,pasoPrueba);
    }

    @Test
    public void crearMatriz3() throws Exception
    {
        String palabras = "ssss,ssss,ssss,ssss";
        SopaDeLetras sopa = new SopaDeLetras(palabras);

        char ideal [][] = 
            {
                {'s','s','s','s'},
                {'s','s','s','s'},
                {'s','s','s','s'},
                {'s','s','s','s'}
            };

        char esperado [][] = sopa.getSopas();  

        boolean pasoPrueba = sonIguales(ideal,esperado);
        assertEquals(true,pasoPrueba);
    }

    /*
     * se espera que las matrices no sean iguales
     */
    @Test
    public void crearMatriz_conError1() throws Exception
    {
        String palabras = "ssss,ssss,ssss,ssss";
        SopaDeLetras sopa = new SopaDeLetras();

        char ideal [][] = 
            {
                {'s','s','s','s'},
                {'s','s','s','s'},
                {'s','s','s','s'},
                {'s','s','s','s'}
            };

        char esperado [][] = sopa.getSopas();  

        boolean pasoPrueba = sonIguales(ideal,esperado);
        assertEquals(false,pasoPrueba);
    }

    @Test
    public void crearMatriz_conError2() throws Exception
    {
        String palabras = "jairo";
        SopaDeLetras sopa = new SopaDeLetras(palabras);

        char ideal [][] = 
            {
                {'s','s','s','s'},
                {'s','s','s','s'},
                {'s','s','s','s'},
                {'s','s','s','s'}
            };

        char esperado [][] = sopa.getSopas();  

        boolean pasoPrueba = sonIguales(ideal,esperado);
        assertEquals(false,pasoPrueba);
    }

    /*
     * se espera que la matriz sea cuadrada
     */
    @Test
    public void verificarMatrizCuadrada1() throws Exception
    {
        String palabras = "ssss,ssss,ssss,ssss";
        SopaDeLetras sopa = new SopaDeLetras(palabras); 
        boolean pasoPrueba = sopa.esCuadrada();
        assertEquals(true,pasoPrueba);
    }

    @Test
    public void verificarMatrizCuadrada2() throws Exception
    {
        String palabras = "as,sa";
        SopaDeLetras sopa = new SopaDeLetras(palabras); 
        boolean pasoPrueba = sopa.esCuadrada();
        assertEquals(true,pasoPrueba);
    }

    @Test
    public void verificarMatrizCuadrada3() throws Exception
    {
        String palabras = "asas,sasa,sass,aass";
        SopaDeLetras sopa = new SopaDeLetras(palabras); 
        boolean pasoPrueba = sopa.esCuadrada();
        assertEquals(true,pasoPrueba);
    }

    /*
     * se espera que la matriz NO sea cuadrada
     */
    @Test
    public void verificarMatrizCuadrada4() throws Exception
    {
        String palabras = "sasa,sass,aass";
        SopaDeLetras sopa = new SopaDeLetras(palabras); 
        boolean pasoPrueba = sopa.esCuadrada();
        assertEquals(false,pasoPrueba);
    }

    @Test
    public void verificarMatrizCuadrada5() throws Exception
    {
        String palabras = "1234";
        SopaDeLetras sopa = new SopaDeLetras(palabras); 
        boolean pasoPrueba = sopa.esCuadrada();
        assertEquals(false,pasoPrueba);
    }

    /*
     * se espera que la matriz sea dispersa
     */
    @Test
    public void verificarMatrizDispersa1() throws Exception
    {
        String palabras = "sasa,sass,aas";
        SopaDeLetras sopa = new SopaDeLetras(palabras); 
        boolean pasoPrueba = sopa.esDispersa();
        assertEquals(true,pasoPrueba);
    }

    @Test
    public void verificarMatrizDispersa2() throws Exception
    {
        String palabras = "saass,s alf";
        SopaDeLetras sopa = new SopaDeLetras(palabras); 
        boolean pasoPrueba = sopa.esDispersa();
        assertEquals(true,pasoPrueba);
    }

    @Test
    public void verificarMatrizDispersa3() throws Exception
    {
        String palabras = "sa sa,ss,a a s s";
        SopaDeLetras sopa = new SopaDeLetras(palabras); 
        boolean pasoPrueba = sopa.esDispersa();
        assertEquals(true,pasoPrueba);
    }

    @Test
    public void verificarMatrizDispersa4() throws Exception
    {
        String palabras = "0987,0000";
        SopaDeLetras sopa = new SopaDeLetras(palabras); 
        boolean pasoPrueba = sopa.esDispersa();
        assertEquals(false,pasoPrueba);
    }

    @Test
    public void verificarMatrizDispersa5() throws Exception
    {
        String palabras = "0987,0000,1234,5555";
        SopaDeLetras sopa = new SopaDeLetras(palabras); 
        boolean pasoPrueba = sopa.esDispersa();
        assertEquals(false,pasoPrueba);
    }

    @Test
    public void verificarMatrizRectangular1() throws Exception
    {
        String palabras = "rectangulo,rectangulo";
        SopaDeLetras sopa = new SopaDeLetras(palabras); 
        boolean pasoPrueba = sopa.esRectangular();
        assertEquals(true,pasoPrueba);
    }

    @Test
    public void verificarMatrizRectangular2() throws Exception
    {
        String palabras = "re cta ngul o,rec ta ng ulo";
        SopaDeLetras sopa = new SopaDeLetras(palabras); 
        boolean pasoPrueba = sopa.esRectangular();
        assertEquals(true,pasoPrueba);
    }

    @Test
    public void verificarMatrizRectangular3() throws Exception
    {
        String palabras = "reck,rect,tera,giga,beta,alfa,omfg";
        SopaDeLetras sopa = new SopaDeLetras(palabras); 
        boolean pasoPrueba = sopa.esRectangular();
        assertEquals(true,pasoPrueba);
    }

    @Test
    public void verificarMatrizRectangular4() throws Exception
    {
        String palabras = "cuadro,cuadro,cuadro,cuadro,cuadro,cuadro";
        SopaDeLetras sopa = new SopaDeLetras(palabras); 
        boolean pasoPrueba = sopa.esRectangular();
        assertEquals(false,pasoPrueba);
    }

    @Test
    public void verificarMatrizRectangular5() throws Exception
    {
        String palabras = "dispersa,dis,per,sa";
        SopaDeLetras sopa = new SopaDeLetras(palabras); 
        boolean pasoPrueba = sopa.esRectangular();
        assertEquals(false,pasoPrueba);
    }

    @Test
    public void verificarMatrizDiagonal1() throws Exception
    {
        String palabras = "MARCO,MARCO,MARCO,MARCO,MARCO,";
        SopaDeLetras sopa = new SopaDeLetras(palabras); 
        char ideal[]={'M','A','R','C','O'};
        boolean pasoPrueba = sonIgualesVector(ideal,sopa.getDiagonalPrincipal());
        assertEquals(true,pasoPrueba);
    }

    @Test
    public void verificarMatrizDiagonal2() throws Exception
    {
        String palabras = "012,012,012";
        SopaDeLetras sopa = new SopaDeLetras(palabras); 
        char ideal[]={'0','1','2'};
        boolean pasoPrueba = sonIgualesVector(ideal,sopa.getDiagonalPrincipal());
        assertEquals(true,pasoPrueba);
    }

    @Test
    public void verificarMatrizDiagonal3() throws Exception
    {
        String palabras = "m r c,c r m,mcr";
        SopaDeLetras sopa = new SopaDeLetras(palabras); 
        char ideal[]={'m','r','r'};
        boolean pasoPrueba = sonIgualesVector(ideal,sopa.getDiagonalPrincipal());
        assertEquals(true,pasoPrueba);
    }

    @Test
    public void verificarMatrizDiagonal4() throws Exception
    {
        String palabras = "999,900,999";
        SopaDeLetras sopa = new SopaDeLetras(palabras); 
        char ideal[]={'3','3','3'};
        boolean pasoPrueba = sonIgualesVector(ideal,sopa.getDiagonalPrincipal());
        assertEquals(false,pasoPrueba);
    }

    @Test
    public void verificarMatrizDiagonal5() throws Exception
    {
        String palabras = "juan,juan,juan,juan";
        SopaDeLetras sopa = new SopaDeLetras(palabras); 
        char ideal[]={'m','a','r','c','o'};
        boolean pasoPrueba = sonIgualesVector(ideal,sopa.getDiagonalPrincipal());
        assertEquals(false,pasoPrueba);
    }

    @Test
    public void verificarMatrizContada1() throws Exception
    {
        String palabras = "mrcoa,marmamamacom,amarmmaco,maaamrco,marmomomaco";
        SopaDeLetras sopa = new SopaDeLetras(palabras); 
        boolean pasoPrueba = 9==sopa.getContar("ma");
        assertEquals(true,pasoPrueba);
    }

    @Test
    public void verificarMatrizContada2() throws Exception
    {
        String palabras = "ella,e lla,el la,ell a";
        SopaDeLetras sopa = new SopaDeLetras(palabras); 
        boolean pasoPrueba = 0==sopa.getContar("t u");
        assertEquals(true,pasoPrueba);
    }

    @Test
    public void verificarMatrizContada3() throws Exception
    {
        String palabras = "ella,ela llae,lel lae,ell a";
        SopaDeLetras sopa = new SopaDeLetras(palabras); 
        boolean pasoPrueba = 4==sopa.getContar("el");
        assertEquals(true,pasoPrueba);
    }

    @Test
    public void verificarMatrizContada4() throws Exception
    {
        String palabras = "mrcoa,marmamamacom,amarmmaco,maaamrco,marmomomaco";
        SopaDeLetras sopa = new SopaDeLetras(palabras); 
        boolean pasoPrueba = 8==sopa.getContar("ma");
        assertEquals(false,pasoPrueba);
    }

    @Test
    public void verificarMatrizContada5() throws Exception
    {
        String palabras = "marco,ocram,ana,hl3m,a";
        SopaDeLetras sopa = new SopaDeLetras(palabras); 
        boolean pasoPrueba = 1!=sopa.getContar("3");
        assertEquals(false,pasoPrueba);
    }

    private boolean sonIguales(char m1[][], char m2[][])
    {

        if(m1==null || m2==null || m1.length != m2.length)
            return false; 

        int cantFilas=m1.length; 

        for(int i=0;i<cantFilas;i++)
        {
            if( !sonIgualesVector(m1[i],m2[i]))
                return false;
        }

        return true;
    }

    private boolean sonIgualesVector(char v1[], char v2[])
    {
        if(v1.length != v2.length)
            return false;

        for(int j=0;j<v1.length;j++)
        {
            if(v1[j]!=v2[j])
                return false;
        }
        return true;
    }
}
